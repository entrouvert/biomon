# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


from django.conf.urls import patterns, url, include
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.i18n import javascript_catalog
from django.contrib.auth.decorators import login_required

from . import views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', login_required(views.PatientList.as_view()), name='patient_list'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', login_required(views.LogoutView.as_view()), name='logout'),
    url(r'^patient/add/$', login_required(views.PatientCreate.as_view()), name='patient_add'),
    url(r'^patient/(?P<pk>\d+)/$', login_required(views.PatientDetail.as_view()), name='patient_detail'),
    url(r'^patient/(?P<pk>\d+)/update/$', login_required(views.PatientUpdate.as_view()), name='patient_update'),
    url(r'^patient/(?P<pk>\d+)/update_enabled_field/$', login_required(views.PatientUpdateEnabledField.as_view()), name='patient_update_enabled_field'),
    url(r'^patient/(?P<pk>\d+)/delete/$', login_required(views.PatientDelete.as_view()), name='patient_delete'),
    url(r'^patient/(?P<pk>\d+)/check_all_alerts$', login_required(views.AllAlertCheckPatientView.as_view()), name='patient_check_all_alerts'),
)

urlpatterns += (
    url(r'^rooms/$', login_required(views.RoomList.as_view()), name='room_list'),
    url(r'^room/add/$', login_required(views.RoomCreate.as_view()), name='room_add'),
    url(r'^room/(?P<pk>\d+)/$', login_required(views.RoomUpdate.as_view()), name='room_update'),
    url(r'^room/(?P<pk>\d+)/delete/$', login_required(views.RoomDelete.as_view()), name='room_delete'),
    url(r'^room-number-to-pid/(?P<number>\w+)', views.RoomNumberToPid.as_view(), name='room_number_to_pid'),
)

urlpatterns += (
    url(r'^readers/$', login_required(views.ReaderList.as_view()), name='reader_list'),
    url(r'^readers/(?P<pk>\d+)/$', login_required(views.ReaderConfig.as_view()), name='reader_config'),
    url(r'^reader/add/$', login_required(views.ReaderCreate.as_view()), name='reader_add'),
    url(r'^reader/(?P<pk>\d+)/$', login_required(views.ReaderUpdate.as_view()), name='reader_update'),
    url(r'^reader/(?P<pk>\d+)/delete/$', login_required(views.ReaderDelete.as_view()), name='reader_delete'),
)

urlpatterns += (
    url(r'^alert/(?P<pk>\d+)/update_checked_field$', login_required(views.AlertCheckView.as_view()), name='alert_checked_field'),
    url(r'^alerts/$', login_required(views.AllAlertsView.as_view()), name='all_alerts'),
    url(r'^alerts/check_all_alerts$', login_required(views.AllAlertsCheckView.as_view()), name='check_all_alerts'),
)

urlpatterns += (url(r'^jsi18n/$', javascript_catalog),)

other_patterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += other_patterns

urlpatterns += patterns('',
            (r'^patient/(?P<pk>\d+)/livedata_provider/', include('biomon.livedata_provider.patient_urls')),
            (r'^alerts/livedata_provider/', include('biomon.livedata_provider.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )
