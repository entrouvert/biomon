# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(r'^render/$', views.RenderView.as_view(), name='render_graphite'),
    url(r'^instants/$', views.InstantsView.as_view(), name='instants'),
    url(r'^means/$', views.MeansView.as_view(), name='means'),
    url(r'^histograms/$', views.HistogramView.as_view(), name='histograms'),
    url(r'^minmax/$', views.MinMaxView.as_view(), name='minmax'),
    url(r'^episodes_opened/$', views.PatientEpisodesOpened.as_view(), name='patient_episodes_opened'),
    url(r'^episodes_closed/$', views.PatientEpisodesClosed.as_view(), name='patient_episodes_closed'),
    url(r'^episodes_stats/$', views.PatientEpisodesStatsView.as_view(), name='patient_episodes_stats'),
    url(r'^next_alert$', views.PatientNextEpisode.as_view(), name='patient_next_alert'),
    url(r'^previous_alert$', views.PatientPreviousEpisode.as_view(), name='patient_previous_alert'),
)
