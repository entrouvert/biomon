# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(r'^episodes_opened/$', views.PatientsEpisodesOpened.as_view(), name='patients_episodes_opened'),
    url(r'^episodes_closed/$', views.PatientsEpisodesClosed.as_view(), name='patients_episodes_closed'),
    url(r'^episodes_stats/$', views.PatientsEpisodesStatsView.as_view(), name='patients_episodes_stats'),
    url(r'^episodes_not_checked/$', views.PatientsEpisodesNotChecked.as_view(), name='patients_episodes_not_checked'),
)
