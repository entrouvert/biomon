# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import simplejson as json

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from . import models


class MainForm(forms.ModelForm):
    class Meta:
        model = models.Patient
        fields = ['firstname', 'lastname', 'sex', 'birthdate', 'monitoring_place',
            'room', 'emergency_contact', 'regular_doctor', 'medical_comment']


class SimpleAlertProfileForm(forms.ModelForm):
    hr_max_critical = forms.IntegerField(
        label=_(u'Maximum critical heartrate'), required=False)
    hr_min_critical = forms.IntegerField(
        label=_(u'Minimum critical heartrate'), required=False)
    hr_max_dangerous = forms.IntegerField(
        label=_(u'Maximum dangerous heartrate'), required=False)
    hr_min_dangerous = forms.IntegerField(
        label=_(u'Minimum dangerous heartrate'), required=False)
    t_max_critical = forms.DecimalField(
        label=_(u'Maximum critical temperature'),
        max_digits=5, decimal_places=2, localize=True, required=False)
    t_min_critical = forms.DecimalField(
        label=_(u'Minimum critical temperature'),
        max_digits=5, decimal_places=2, localize=True, required=False)
    t_max_dangerous = forms.DecimalField(
        label=_(u'Maximum dangerous temperature'),
        max_digits=5, decimal_places=2, localize=True, required=False)
    t_min_dangerous = forms.DecimalField(
        label=_(u'Minimum dangerous temperature'),
        max_digits=5, decimal_places=2, localize=True,required=False)
    duration_hr_max_critical = forms.IntegerField(
        label=_(u'Minimal duration'), required=False)
    duration_hr_min_critical = forms.IntegerField(
        label=_(u'Minimal duration'), required=False)
    duration_hr_max_dangerous = forms.IntegerField(
        label=_(u'Minimal duration'), required=False)
    duration_hr_min_dangerous = forms.IntegerField(
        label=_(u'Minimal duration'), required=False)
    duration_t_max_critical = forms.DecimalField(
        label=_(u'Minimal duration'), required=False)
    duration_t_min_critical = forms.DecimalField(
        label=_(u'Minimal duration'), required=False)
    duration_t_max_dangerous = forms.DecimalField(
        label=_(u'Minimal duration'), required=False)
    duration_t_min_dangerous = forms.DecimalField(
        label=_(u'Minimal duration'), required=False)

    class Meta:
        model = models.Patient
        fields = ['simple_alert_profile', 'alert_profile']
        widgets = {'simple_alert_profile': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(SimpleAlertProfileForm, self).__init__(*args, **kwargs)
        if not settings.DISPLAY_ALERT_PROFILE:
            del self.fields['alert_profile']
        if self.instance and self.instance.get_simple_alert_profile():
            for key, value in self.instance.get_simple_alert_profile().items():
                if value is not None:
                    self.fields[key].initial = value

    def clean(self):
        cleaned_data = super(SimpleAlertProfileForm, self).clean()
        sap = {}
        new_cd = {}
        if 'alert_profile' in cleaned_data:
            new_cd['alert_profile'] = cleaned_data.get('alert_profile')
            del cleaned_data['alert_profile']
        for k in self.fields.items():
            if cleaned_data.get(k[0]):
                sap[k[0]] = cleaned_data.get(k[0])
        cleaned_data = new_cd
        cleaned_data['simple_alert_profile'] = json.dumps(sap)
        return cleaned_data


class TemperatureCheckForm(forms.ModelForm):
    class Meta:
        model = models.TemperatureCheck
        fields = ['date', 'time', 'value']


class HeartrateCheckForm(forms.ModelForm):
    class Meta:
        model = models.HeartrateCheck
        fields = ['date', 'time', 'value']
