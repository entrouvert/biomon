# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import re
import operator


ops = { "<": operator.lt,
    ">": operator.gt,
    "<=": operator.le,
    ">=": operator.ge,
    "=": operator.eq,
    }

def quoted_to_list(episode):
    l = list()
    elements = re.findall(r"[\w.'<>=&|-]+", episode)
    for element in elements:
        try:
            l.append(float(element))
        except ValueError:
            l.append(element)
    return l

def get_elementary_episodes(definition):
    definitions = list()
    if not definition or definition[0] != '(' or definition[-1] != ')':
        return definitions
    new_start, new_end = 0, 0
    found = False
    for pos in range(len(definition)):
        if definition[pos] == '(':
            new_start = pos
        if definition[pos] == ')':
            new_end =  pos
        if new_start and new_end:
            found = True
            definitions.extend(
                get_elementary_episodes(
                    definition[new_start: new_end+1]
                    )
                )
            new_start, new_end = 0, 0
    if not found:
        definitions.append(definition)
    return definitions

def get_unique_elementary_definitions(profile):
    if not profile:
        return list()
    definitions = [(k, value) for k, v in profile.items()
        for value in v]
    first_run = [(k, d) for k, definition in definitions
        for d in get_elementary_episodes(definition)]
    second_run = [[k] + quoted_to_list(definition) for k, definition in set(first_run)]
    third_run = [tuple(definition) for definition in second_run
        if definition[1] not in ('&', '|', '-')]
    return third_run

def definition_list_to_dic(definition_list):
    dic = dict()
    for definition in definition_list:
        level_dic = dic.setdefault(definition[0], dict())
        metric_dic = level_dic.setdefault(definition[1], dict())
        operator_list = metric_dic.setdefault(definition[2], list())
        operator_list.append(definition[3:])
    return dic
