# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


from django.db import models
from django.db.models import Max
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from ..models import TimestampedAbstractModel, Patient


class Episode(TimestampedAbstractModel):
    """
        An episode is the recording of an event detected on sensor data.
        The event is described by the episode definition.

        The definition contains conditions on sensor data and is based on
        first order logic.

        That is complex to structure a recursive data structure for
        persistence, so we store a litteral expression of the definition.
    """
    class Meta:
        verbose_name = _(u'Episode')
        verbose_name_plural = _(u'Episodes')

    start = models.DateTimeField(_(u'Beginning'))
    end = models.DateTimeField(_(u'End'))
    patient = models.ForeignKey('biomon.Patient', verbose_name=_(u'Patient'))
    opened = models.BooleanField(verbose_name=_(u'Opened'), default=True)
    definition = models.CharField(max_length=2048)
    level = models.CharField(max_length=2048, null=True, blank=True)
    metric = models.CharField(max_length=2048, null=True, blank=True)
    seq_id = models.IntegerField(blank=True, null=True)
    checked = models.BooleanField(default=False)
    checker = models.ForeignKey(User, null=True, blank=True)

    def timedelta(self):
        '''
            Distance between start and end of the episode
        '''
        return self.end - self.start

    @property
    def duration(self):
        return self.timedelta().total_seconds()

    def allocate_seq_id(self):
        seq_id = 1
        max_seq_id = Episode.objects.filter(patient=self.patient).\
                aggregate(Max('seq_id'))['seq_id__max']
        if max_seq_id:
            seq_id = max_seq_id + 1
        return seq_id

    def save(self, *args, **kwargs):
        if not self.seq_id:
            self.seq_id = self.allocate_seq_id()
        super(Episode, self).save(*args, **kwargs)
