# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import django
from django.conf.global_settings import *
import logging.config

BASE_DIR = os.path.dirname(__file__)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '43umk@mz8p^4gw&sxyys3cud$tdteyl_jwxn2n1mw&ra6iyohb'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DEBUG_DB = False
TEMPLATE_DEBUG = True
MEDIA = 'media'


ALLOWED_HOSTS = ['127.0.0.1', ]


LOGIN_URL = '/login'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'biomon',
    'biomon.livedata_provider',
    'biomon.medibot',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'biomon.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.static',
            ],
        },
    },
]

STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)

LOCALE_PATHS = ( os.path.join(BASE_DIR, 'locale'), )

#WSGI_APPLICATION = 'biomon.wsgi.application'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(ip)s %(user)s %(request_id)s %(levelname)s %(name)s.%(funcName)s: %(message)s',
            'datefmt': '%Y-%m-%d %a %H:%M:%S'
        },
        'verbose_db': {
            'format': '[%(asctime)s] - - - %(levelname)s %(name)s.%(funcName)s: %(message)s',
            'datefmt': '%Y-%m-%d %a %H:%M:%S'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'verbose',
        },
	# remove request_context filter for db log to prevent infinite loop
	# when logging sql query to retrieve the session user
        'console_db': {
            'level': 'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'verbose_db',
        },
    },
    'loggers': {
        # even when debugging seeing SQL queries is too much, activate it
        # explicitly using DEBUG_DB
        'django.db': {
                'level': 'INFO',
                'handlers': ['console_db'],
                'propagate': False,
        },
        # django_select2 outputs debug message at level INFO
        'django_select2': {
                'level': 'WARNING',
        },
        '': {
                'handlers': ['console'],
                'level': 'INFO',
        },
    },
}

# Post local config setting
if DEBUG:
    LOGGING['loggers']['']['level'] = 'DEBUG'
if DEBUG_DB:
    LOGGING['loggers']['django.db']['level'] = 'DEBUG'
logging.config.dictConfig(LOGGING)

if 'BIOMON_SETTINGS_FILE' in os.environ:
    execfile(os.environ['BIOMON_SETTINGS_FILE'])

GRAPHITE_HOST = ""
WHISPER_PATH = ""
WHISPER_METRIC_PATH = "entrouvert.research.veadista.patients.monitoring"
WHISPER_HEARTRATE_METRIC = "heartrate"
WHISPER_TEMPERATURE_METRIC = "temperature"
WHISPER_FILE_EXTENSION = '.wsp'
SENSOR_MAPPING = {
    'HR' : 'heartrate',
    'T' : 'temperature',
}
DISPLAY_ALERT_PROFILE = True
