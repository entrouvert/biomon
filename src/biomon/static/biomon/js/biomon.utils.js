/*
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var UTILS = UTILS || (function(){
    /*
        Helper functions to build query strings
    */
    function dict_to_query(target, dict){
        for(var key in dict){
            if(dict[key]){
                target += key + "=" + dict[key] + "&";
            }
        };
        return target;
    };

    function fresh_query(target){
        target += "fresh=" + new Date().getTime();
        return target;
    };

    /*
        Helper function to a friendly time display
    */
    function get_display_time_s(seconds){
        if (seconds < 60) {
            return seconds + " s";
        }
        var sec = seconds % 60 ;
        var minutes = (seconds - sec)/60;
        res = get_display_time_min(minutes);
        if (sec) {
            res += " " + sec + " s";
        }
        return res;
    };

    function get_display_time_min(minutes){
        if (minutes < 60) {
            return minutes + " min";
        }
        var mins = minutes % 60 ;
        var hours = (minutes - mins)/60;
        res = get_display_time_hour(hours);
        if (mins) {
            res += " " + mins + " min";
        }
        return res;
    };

    function get_display_time_hour(hours){
        if (hours < 24) {
            return hours + " h";
        }
        var h = hours % 24 ;
        var days = (hours - h)/24;
        res = days + " j";
        if (h) {
            res += " " + h + " h";
        }
        return res;
    };

    function show_message(message, duration) {
        duration = duration || 1000;
        $('div#wrapper-0').append('<ul class="messages"><li>' + message + '</li></ul>');
        $('.messages').delay(duration).fadeOut(1000);
    }

    return {
        dict_to_query: dict_to_query,
        fresh_query: fresh_query,
        get_display_time_s: get_display_time_s,
        get_display_time_min: get_display_time_min,
        get_display_time_hour: get_display_time_hour,
        show_message: show_message,
        }

})();
