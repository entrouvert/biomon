/*
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var ALERTS = ALERTS || (function(){

    var
        /* Flag for displaying checked episodes */
        _show_checked = false,
        /* Flag for displaying running episodes */
        _show_running = true,
        /* Flag for displaying closed episodes */
        _show_closed = true,
        /* Livedata provider URLs*/
        _alerts_stats_url = "livedata_provider/episodes_stats?",
        _opened_alerts_url = "livedata_provider/episodes_opened?",
        _closed_alerts_url = "livedata_provider/episodes_closed?",
        /* datetime of last refreshed closed alert */
        _after = null,
        /* Handlers of timeout */
        _refresh_alerts_to = null,
        _refresh_alerts_stats_to = null;

    function _display_div_date(){
        $("div.date").each(function() {
            var wrap = $(this);
            wrap.show();
            if( wrap.find("tr.alert-block:visible").length == 0 ) {
                wrap.hide();
            }
        });
    }

    /*
    HTML for displaying episodes building functions :
        * _build_alert_block to build a table row, one for each episode
    */
    function _build_alert_block(url, opened, alert) {
        var
            /* Unpack alert Array */
            seq_id = alert[0],
            level = alert[3][0],
            metric = alert[3][1],
            operator = alert[3][2],
            threshold = alert[3][3],
            duration = alert[4],
            start_date = alert[1],
            end_date = opened ? "" : alert[2],
            checked = alert[6],
            id = alert[7],
            patient_id = alert[8],
            patient_display_name = alert[9],
            patient_monitoring_place = alert[10],
            /* html content in a string */
            content = '<tr class="alert-block ';

        if ( opened ) {
            content += 'opened ';
        }
        if ( checked ) {
            content += 'checked ';
        }
        content += level.toLowerCase() + '" href="' + url + '" id="' + id + '" start="' + new Date(start_date) + '" end="';
        if ( end_date ) {
            content += new Date(end_date)
        }
        if ( patient_id ) {
            content += '" patient-id="' + patient_id;
        }
        content += '">';
        if ( patient_id ) {
            content += '<td>' + patient_display_name + '</td>';
        }
        content += '<td>';
        if ( metric == 'HR' ){
            content += '<i class="fa fa-heart"></i>';
        } else if ( metric == 'T' ){
            content += '<i class="wi wi-thermometer-exterior"></i>';
        } else {
            content += '<i class="fa fa-question"></i>';
        }
        content += '</td><td>' + operator + ' ' + threshold + '</td>'
        content += '<td>';
        if ( end_date ) {
            s = $.format.date(new Date(start_date), "dd/MM/yyyy");
            e = $.format.date(new Date(end_date), "dd/MM/yyyy");
            if (s != e) {
                s = $.format.date(new Date(start_date), "(dd/MM/yyyy) HH:mm:ss");
            } else {
                s = $.format.date(new Date(start_date), "HH:mm:ss");
            }
            e = $.format.date(new Date(end_date), "HH:mm:ss");
            content += '<span class="value">' + s + '</span>';
            content += ' <i class="fa fa-long-arrow-right"></i> ';
            content += '<span class="value"> ' + e + '</span>';
        } else {
            content += '<span class="value"> '
                + $.format.date(new Date(start_date), "HH:mm:ss")+'</span>';
        }
        content += '</td>';
        content += '<td class="duration">' + UTILS.get_display_time_s(duration) + '</td>';
        if ( patient_id ) {
            content += '<td>' + patient_monitoring_place + '</td>';
        }
        if ( checked ) {
            content += '<td class="checker checked">' + gettext('Checked') + '</td>'
        } else {
            content += '<td class="checker unchecked">' + gettext('Not checked') + '</td>'
        }
        content += '</tr>';
        return content;
    };

    function refresh_alerts_stats(for_patient){
        var target = _alerts_stats_url;
        target = UTILS.fresh_query(target);
        $.ajax( {
            type: 'Get',
            url: target,
            success: function(data) {
                $('span#critical_alert_count').empty();
                $('span#dangerous_alert_count').empty();
                $('span#display_name').removeClass();
                $('a#tab-alert').removeClass('dangerous critical');
                var level = "";
                if( data[2] > 0 ){
                    $('span#dangerous_alert_count').append('<i class="fa fa-warning"></i> ' + data[2]);
                    if(data[3]){
                        level = "dangerous"
                    }
                }
                if( data[0] > 0 ){
                    $('span#critical_alert_count').append('<i class="fa fa-warning"></i> ' + data[0]);
                    if(data[1]){
                        level = "critical"
                    }
                }
                if( for_patient ){
                    if(level){
                        $('span#display_name').addClass(level);
                        $('a#tab-alert').addClass(level);
                        $('span#display_name, a#tab-alert').fadeOut(500);
                        $('span#display_name, a#tab-alert').fadeIn(1500);
                    }
                }
            },
            error: function() {
                $('span#critical_alert_count').empty();
                $('span#dangerous_alert_count').empty();
                UTILS.show_message(gettext("Database offline."));
            }
        });
        _refresh_alerts_stats_to = setTimeout(function(){
            refresh_alerts_stats(for_patient);
        }, 2000);
    };

    function stop_refresh_alerts_stats(){
        clearTimeout(_refresh_alerts_stats_to);
    };

    /*
    Grab episodes in ajax :
        * refresh all running episodes
        * Only request new closed episodes using var after
    */
    function refresh_alerts(){
        var opened = [];
        var closed = [];
        var target_opened = UTILS.fresh_query(_opened_alerts_url);
        var target_closed = _closed_alerts_url;
        if (_after) {
            /* Encode date in iso format */
            target_closed += $.param({after: _after}) + '&'
        }
        target_closed = UTILS.fresh_query(target_closed);
        function get_opened(target) {
            return $.ajax({
                type: 'Get',
                url: target,
                success: function(data) {
                    opened = data.slice();
                },
                error: function() {
                    UTILS.show_message(gettext("Database offline."));
                }
            })
        }
        function get_closed(target) {
            return $.ajax({
                type: 'Get',
                url: target,
                success: function(data) {
                    closed = data.slice();
                },
                error: function() {
                    UTILS.show_message(gettext("Database offline."));
                }
            })
        }
        function run(){
            $.when(get_opened(target_opened), get_closed(target_closed)).done(function(a1, a2){
                opened.length ?
                    $('div#running-alerts span#running').show() :
                    $('div#running-alerts span#running').hide();
                if ( closed.length ) {
                    $('div#closed-alerts span#closed').show();
                }
                $('div#running-alerts').find('*').not('span#running').not('i.fa-play').remove();
                var
                    current_date = "",
                    content = "",
                    alert;
                for ( var i = 0; i < opened.length; i++ ) {
                    alert = opened[i];
                    if ( current_date != alert[5] ){
                        current_date = alert[5];
                        d = $.format.date(new Date(alert[5]), "dd/MM/yyyy");
                        today = $.format.date(new Date(), "dd/MM/yyyy");
                        if ( d == today ) {
                            d = gettext('Today');
                        }
                        if( content ){
                            content += '</table></div>'
                        }
                        content += '<div class="date" id="running-'
                                + alert[5]
                                + '"><div class="date_value">'
                                + gettext('Start') + ' : ' + d
                                + '</div><table>';
                    };
                    content += _build_alert_block('#', true, alert);
                }
                if ( content ) {
                    content += '</table></div>'
                    $('div#running-alerts').append(content);
                }

                var
                    content_prepend = "",
                    div_preprend_date = "";
                current_date = "";
                content = ""
                for ( var i = 0; i < closed.length; i++ ) {
                    alert = closed[i];
                    if ( $('div#closed-alerts div#closed-' +alert[5]).length ) {
                        div_preprend_date = alert[5];
                        content_prepend += _build_alert_block('#', false, alert);
                    } else {
                        if ( current_date != alert[5] ){
                            current_date = alert[5];
                            d = $.format.date( new Date(alert[5]), "dd/MM/yyyy" );
                            today = $.format.date( new Date(), "dd/MM/yyyy" );
                            if ( d == today ) {
                                d = gettext( 'Today' );
                            }
                            if( content ){
                                content += '</table></div>'
                            };
                            content += '<div class="date" id="closed-'
                                    + alert[5]
                                    + '"><div class="date_value">'
                                    + gettext('Start') + ' : ' + d
                                    + '</div><table>';
                        };
                        content += _build_alert_block('#', false, alert);
                    }
                }
                if( content ){
                    content += '</table></div>'
                    $('div#closed-alerts span#closed').after(content);
                };
                if( content_prepend ){
                    $( 'div#closed-alerts div#closed-' + div_preprend_date + ' table' ).prepend( content_prepend );
                };
                if ( closed.length ){
                    _after = closed[0][2];
                }

                if ( _show_running ) {
                    $('div#running-alerts').show();
                } else {
                    $('div#running-alerts').hide();
                }
                if ( _show_closed ) {
                    $('div#closed-alerts').show();
                } else {
                    $('div#closed-alerts').hide();
                }
                if ( !_show_checked ) {
                    $('tr.checked').hide();
                }

                _display_div_date();
            });
        };
        run();
        _refresh_alerts_to = setTimeout(function(){
            refresh_alerts();
        }, 2000);
    };

    function stop_refresh_alerts(){
        clearTimeout(_refresh_alerts_to);
    };

    /*
        Episode display controls
    */
    $(document).on('click', 'div.tab-nav-bar', function(e) {
        target = $(e.target);
        if ( target.is("i#show-running") ) {
            if ( target.hasClass("enabled") ) {
                target.switchClass("enabled", "disabled", 0);
                _show_running = false;
                $('div#running-alerts').hide();
            } else {
                target.switchClass("disabled", "enabled", 0);
                _show_running = true;
                $('div#running-alerts').show();
            }
        } else if ( target.is("i#show-closed") ) {
            if ( target.hasClass("enabled") ) {
                target.switchClass("enabled", "disabled", 0);
                _show_closed = false;
                $('div#closed-alerts').hide();
            } else {
                target.switchClass("disabled", "enabled", 0);
                _show_closed = true;
                $('div#closed-alerts').show();
            }
        } else if ( target.is("i#show-checked") ) {
            if( target.hasClass("enabled") ) {
                target.switchClass("enabled", "disabled", 0);
                _show_checked = false;
                $('tr.checked').hide();
            } else {
                target.switchClass("disabled", "enabled", 0);
                _show_checked = true;
                $('tr.checked').show();
            }
            _display_div_date();
        } else if ( target.is("i#check-all") ) {
            $.ajax({
                type: 'post',
                url: 'check_all_alerts',
                data: {},
                success: function(data) {
                    if(data['message'] == 'ok'){
                        /* check all episodes !*/
                        $('td.checker.unchecked').each(function() {
                            $(this).switchClass("unchecked", "checked", 0);
                            $(this).text(gettext('Checked'));
                            $(this).parent().addClass("checked");
                        });
                        UTILS.show_message(gettext("All episodes are checked"));
                        if (!show_checked) {
                            $('tr.checked').fadeOut('slow');
                        }
                    } else {
                        UTILS.show_message(gettext("The request for checking all alerts has failed."));
                    }
                    _display_div_date();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    UTILS.show_message(gettext("The request for checking all alerts has failed."));
                }
            });
        }
    });

    /*
    Clicking on episode controls :
        * checking episode with last td
        * goto episode on dashboard on other td
    */
    $(document).on('click', 'tr.alert-block', function(e) {
        var target = $(e.target);
        if (target.is("td.checker")){
            var value = '';
            if (target.hasClass('unchecked')) {value = 'on';}
            $.ajax({
                type: 'post',
                url: '/alert/' + target.parent().attr('id') + '/update_checked_field',
                data: {checked: value},
                success: function(data) {
                    if(value == 'on'){
                        target.switchClass("unchecked", "checked", 0);
                        target.text(gettext('Checked'));
                        target.parent().addClass("checked");
                    } else {
                        target.switchClass("checked", "unchecked", 0);
                        target.text(gettext('Not checked'));
                        target.parent().removeClass("checked");
                    };
                    UTILS.show_message(gettext("Episode updated."));
                    if (!_show_checked) {
                        $('tr.checked').fadeOut('slow');
                    }
                    _display_div_date();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    UTILS.show_message(gettext("Episode not updated !"));
                }
            });
        } else if ( window.location.pathname == '/alerts/') {
            //XXX We should set frame to alert when loading page on patient tab et and click on the dashboard.
            window.location.href = '/patient/' + $(this).attr('patient-id');
        } else {
            var start = Math.floor(Date.parse($(this).attr('start'))/1000);
            var end = Date.parse($(this).attr('end'));
            console.log(start);
            console.log(end);
            if (end) {
                /*
                    The episode is closed.
                    We show the dashboard beginning at the beginning of the event.
                    The range cover the whole episode.
                    The player is in pause.
                */
                end = Math.floor(end/1000);
                $('a#tab-dashboard').click(); // play
                DASHBOARD.set_frame(end, start);
            } else {
                /*
                    The episode is running.
                    We show the dashboard ending now.
                    The range cover the whole episode.
                    The player is playing.
                */
                end = Math.floor(new Date()/1000);
                duration = end - start;
                DASHBOARD.set_frame(end, start);
                $('a#tab-dashboard').click(); // play live
            }
        }
    });

    return {
        refresh_alerts_stats: refresh_alerts_stats,
        stop_refresh_alerts_stats: stop_refresh_alerts_stats,
        refresh_alerts: refresh_alerts,
        stop_refresh_alerts: stop_refresh_alerts
    }
})();
