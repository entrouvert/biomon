/*
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function($) {
  $(function() {
    /*
        Patient searching
    */
    var patient_enabled = true;
    function show_letters() {
        $('.letter').each(function(){
            $(this).show();
        });
    };
    function hide_letters() {
        $('.letter').each(function(){
            ($(this).children('div.list').children(':visible').length == 0) ? $(this).hide() : $(this).show();
        });
    };
    function filter_patients(input) {
        show_letters();
        $('.patient-block').each(function(){
            var text = $(this).attr('display-name').toLowerCase();
            (text.indexOf(input) == 0
                & ((patient_enabled & $(this).hasClass("enabled"))
                    | (!(patient_enabled) & $(this).hasClass("disabled"))))
                        ? $(this).fadeIn() : $(this).hide();
        });
        hide_letters();
    };
    $('input#search').keyup(function(){
        var input = $(this).val().toLowerCase();
        if (input != '') {
            $('i#filter-patient-eraser').show();
        } else {
            $('i#filter-patient-eraser').hide();
        }
        filter_patients(input);
    });
    $('i#filter-patient-eraser').click(function() {
        $('input#search').val("");
        $('i#filter-patient-eraser').hide();
        filter_patients("");
    });
    $('span#filter-patient-enabled').click(function() {
        if($(this).hasClass("enabled")) {
            $(this).switchClass("enabled", "disabled", 0);
            $(this).children().first().switchClass("fa-folder-open enabled", "fa-folder disabled", 0);
            $(this).children().last().switchClass("fa-folder disabled", "fa-folder-open enabled", 0);
            patient_enabled = false;
            show_letters();
            var input = $('input#search').val().toLowerCase();
            filter_patients(input);
            hide_letters();
        } else {
            $(this).switchClass("disabled", "enabled", 0);
            $(this).children().first().switchClass("fa-folder disabled", "fa-folder-open enabled", 0);
            $(this).children().last().switchClass("fa-folder-open enabled", "fa-folder disabled", 0);
            patient_enabled = true;
            show_letters();
            var input = $('input#search').val().toLowerCase();
            filter_patients(input);
            hide_letters();
        }
    });
    $('i#filter-patient-eraser').hide();
    $("div.patient-block.disabled").hide();
    hide_letters();

    /*
        Open/Close patient file

        Don't try to grab event on i. It would mean
        cheat with stopPropagation and preventDefault to avoid
        the redirect due to the parent div click event.
    */
    $('div.patient-block').click(function(e) {
        var target = $(e.target);
        if (target.is("i") & target.hasClass("patients-enabler")){
            var value = '';
            if (target.hasClass('disabled')) {value = 'on';}
            $.ajax({
                type: 'post',
                url: target.attr('action'),
                data: {enabled: value},
                success: function() {
                    if(value == ''){
                        target.switchClass("fa-folder-open enabled", "fa-folder disabled", 0);
                        target.parent().switchClass("enabled", "disabled", 0);
                        target.next().addClass("disabled");
                    } else {
                        target.switchClass("fa-folder disabled", "fa-folder-open enabled", 0);
                        target.parent().switchClass("disabled", "enabled", 0);
                        target.next().removeClass("disabled");
                    };
                    target.parent().fadeOut("slow", function(){
                        hide_letters();
                        UTILS.show_message(gettext("Patient record updated."));
                    });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    UTILS.show_message(gettext("Patient record not updated !"));
                }
            });
        } else {
            window.location.href = $(this).attr('action');
        }
    });
  })
})(jQuery);
