/*
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function($) {
  $(function() {

    var
        _alerts_not_checked_url = "/alerts/livedata_provider/episodes_not_checked?",
        _login_page = false;

    if(window.location.pathname == '/login/') {
        _login_page = true;
    }

    /*
        Common settings
    */
    $("button").button();
    $('button#logout').click(function(){
        window.location.href=$(this).attr('target');
    });
    $("select").selectmenu();
    $('div#controlbar').hide();
    $('div#content-1').css({height: "100%"});

    /*
        Refresh datetime in header
    */
    function refresh_now(){
        $('span#now').text($.format.date(new Date(), "dd/MM/yyyy HH:mm:ss"));
        setTimeout(function(){
            refresh_now();
        }, 1000);
    };
    refresh_now();

    /*
        Ajax setup for Django protected views

        See https://docs.djangoproject.com/en/dev/ref/csrf/#ajax
        for Django CSRF protection
    */
    var csrftoken = $.cookie('csrftoken');
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
    });

    var _after;
    function _build_alert_block(alert){
        var
            seq_id = alert[0],
            level = alert[3][0],
            metric = alert[3][1],
            operator = alert[3][2],
            threshold = alert[3][3],
            duration = alert[4],
            start_date = alert[1],
            end_date = alert[2],
            checked = alert[6],
            id = alert[7],
            patient_id = alert[8],
            patient_display_name = alert[9],
            patient_monitoring_place = alert[10];
        content = '<div class="alert-message '+ level.toLowerCase()
                + '" patient-id="' + patient_id + '">'
                + '<p><span id="name">' + patient_display_name + '</span></p>'
                + '<p><span id="location">' + patient_monitoring_place + '</span></p>'
                + '<p><span id="name">';

        if ( metric == 'HR' ){
            content += '<i class="fa fa-heart"></i>';
        } else if ( metric == 'T' ){
            content += '<i class="wi wi-thermometer-exterior"></i>';
        } else {
            content += '<i class="fa fa-question"></i>';
        }
        content += ' ' + operator + ' ' + threshold
                + '</span></p>'
                + '</div>';
        return content;
    };
    function _refresh_all_alerts(){
        var target = _alerts_not_checked_url;
        if (_after) {
            /* Encode date in iso format */
            target += $.param({after: _after}) + '&'
        }
        target = UTILS.fresh_query(target);
        var not_checked = [];
        function get_not_checked(target) {
            return $.ajax({
                type: 'Get',
                url: target,
                success: function(data) {
                    not_checked = data.slice();
                },
                error: function() {
                    UTILS.show_message(gettext("Database offline."));
                }
            })
        }

        function run(){
            $.when(get_not_checked(target)).done(function(a1){
                var content = ''
                if ( not_checked.length ){
                    _after = not_checked[0][2];
                    for ( var i = 0; i < not_checked.length; i++ ) {
                         content += _build_alert_block(not_checked[i]);
                    }
                    $('div#controlbar').empty();
                    $('div#controlbar').append(content);
                    $('div#content-1').height('88%');
                    $('div#controlbar').fadeIn();
                } else {
                    $('div#controlbar').fadeOut(500);
                    $('div#content-1').height('100%');
                }
            });
        }
        run()
        setTimeout(function(){
            _refresh_all_alerts();
        }, 2000);
    };
    if(!_login_page){
        _refresh_all_alerts();
    }
    $(document).on('click', 'div.alert-message', function(e) {
        window.location.href = '/patient/' + $(this).attr('patient-id');
    });
  })
})(jQuery);
