/*
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function($) {
  $(function() {
    /*
        Last visited tab
    */
    $("div#tabs").tabs();
    $('a.patient-tab').click(function() {
        $.cookie('patient-last-tab', $(this).attr('id'), { path: location.pathname });
        var tab_active = $(this).attr('id');
        if ( tab_active == 'tab-alert' ) {
            ALERTS.refresh_alerts();
        } else {
            ALERTS.stop_refresh_alerts();
        }
        if ( tab_active == 'tab-dashboard' ) {
            DASHBOARD.refresh(true);
        } else {
            DASHBOARD.stop_refresh(true);
        }
    });
    if ($.cookie('patient-last-tab')) {
        $('a#' + $.cookie('patient-last-tab')).click();
    } else {
        $('a#tab-dashboard').click();
    };

    /*
        Open/close patient file

        $(this) must be referenced for used in ajax callback
        functions : http://bugs.jquery.com/ticket/7624
    */
    $('i.patient-enabler').click(function() {
        var target = $(this);
        var value = '';
        if (target.hasClass('disabled')) {value = 'on';}
        $.ajax({
            type: 'post',
            url: target.attr('action'),
            data: {enabled: value},
            success: function() {
                if(value == ''){
                    target.switchClass("fa-folder-open enabled", "fa-folder disabled", 0);
                } else {
                    target.switchClass("fa-folder disabled", "fa-folder-open enabled", 0);
                };
                UTILS.show_message(gettext("Patient record updated."));
            },
            error: function(xhr, ajaxOptions, thrownError) {
                UTILS.show_message(gettext("Patient record not updated !"));
            }
        });
    });

    /*
        Refresh open events figures in identification bar
    */
    ALERTS.refresh_alerts_stats(true);

    /*
        Alert tab
    */

    /* At beginning hide titles, if episodes they'll be displayed in fine */
    $('div#running-alerts span#running').hide();
    $('div#closed-alerts span#closed').hide();
  })
})(jQuery);

