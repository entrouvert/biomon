/*
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function($) {
  $(function() {
    function set_element(element, value, new_class){
        element.text(value);
        element.removeClass();
        element.addClass(new_class);
    }

    function _reload_readers(){
        $('div.reader-block').each(function(){
            var target = $(this).attr('id')
            target += "?"
            target = UTILS.fresh_query(target);
            var element = this;
            $.ajax({
                type: 'get',
                url: target,
                success: function(data) {
                    if (data.status == 'connected' && data.config) {
                        $('p#title', element).switchClass('disabled', 'enabled')
                        set_element($('p#address span#status', element), gettext("Connected"), 'enabled')
                        if(data.config.reader_status){
                            set_element($('div.reader-connected p#reader_driver span#status', element), gettext("On"), 'enabled')
                            if(data.config.tags_in_field){
                                set_element($('div.reader-connected p#tags_in_field span#status', element), gettext(data.config.tags_in_field), 'enabled')
                            } else {
                                set_element($('div.reader-connected p#tags_in_field span#status', element), gettext("No tag detected"), 'disabled')
                            }
                            $('div.reader-connected p#tags_in_field', element).fadeIn();
                        } else {
                            set_element($('div.reader-connected p#reader_driver span#status', element), gettext("Off"), 'disabled')
                            $('div.reader-connected p#tags_in_field', element).fadeOut();
                        }
                        if(data.config.transmitter_status){
                            set_element($('div.reader-connected p#transmitter span#status', element), gettext("On"), 'enabled')
                        } else {
                            set_element($('div.reader-connected p#transmitter span#status', element), gettext("Off"), 'disabled')
                        }
                        $('div.reader-connected', element).fadeIn();
                    } else {
                        $('p#title', element).switchClass('enabled', 'disabled')
                        set_element($('p#address span#status', element), gettext("Disconnected"), 'disabled')
                        $('div.reader-connected', element).fadeOut();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    UTILS.show_message(gettext("Connection error."));
                }
            });
        });
    }

    function refresh_readers(){
        _reload_readers();
        setTimeout(function(){
            refresh_readers();
        }, 2000);
    };
    refresh_readers();
    $('div.reader-block').click(function() {
        window.location.href = $(this).attr('action');
    });
  })
})(jQuery);
