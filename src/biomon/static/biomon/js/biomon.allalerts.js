/*
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function($) {
  $(function() {
    $('div#controlbar').remove();
    $('div#content-1').css({'height': '100%'});
    $('div#alerts').css({'height': '94%'});

    /* At beginning hide titles, if episodes they'll be displayed in fine */
    $('div#running-alerts span#running').hide();
    $('div#closed-alerts span#closed').hide();

    /* Run alert stats updating */
    ALERTS.refresh_alerts_stats();

    /* Run alert table updating */
    ALERTS.refresh_alerts();
  })
})(jQuery);
