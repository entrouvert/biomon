# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import os
import time
from datetime import datetime

import whisper

from django.conf import settings

from . import time_utils

class WhisperBackend():

    def __init__(self, subject, metric):
        path = settings.WHISPER_PATH
        path  = os.path.join(path, subject)
        self.fn = os.path.join(path, metric + settings.WHISPER_FILE_EXTENSION)

    def exists(self):
        if os.path.isfile(self.fn):
            return True
        return False

    def get_raw_data(self, duration = 0, until=None):
        if not self.exists():
            return None
        if not until:
            until = self.now()
        _from = until - duration
        return whisper.fetch(self.fn, _from, until)

    def get_data(self, _from, until=None):
        if not self.exists():
            return None
        data = whisper.fetch(self.fn, _from, until)
        if not data or not data[1]:
            return None
        data = [it for it in data[1] if it]
        return data

    def get_timestamped_data(self, duration = 0, until=None):
        data = self.get_raw_data(duration, until)
        res = []
        if not data or not data[1]:
            return res
        start, end, step = data[0]
        for value in data[1]:
            if value:
                res.append((datetime.fromtimestamp(start + step), value))
            start += step
        return res

    def now(self):
        return int(time.time())

    def get_last_value(self, until=None, lookup_range=60):
        if not until:
            until = self.now()
        _from = until - lookup_range # Last data in the past lookup_range duration
        values = self.get_data(_from, until)
        if values:
            return values[-1]
        return None

    def get_mean(self, duration, until=None):
        if not duration:
            return None
        if not until:
            until = self.now()
        _from = until - duration
        values = self.get_data(_from, until)
        if values:
            return sum(values)/len(values)
        return None

    def get_max(self, duration, until=None):
        if not duration:
            return None
        if not until:
            until = self.now()
        _from = until - duration
        values = self.get_data(_from, until)
        if values:
            return max(values)
        return None

    def get_min(self, duration, until=None):
        if not duration:
            return None
        if not until:
            until = self.now()
        _from = until - duration
        values = self.get_data(_from, until)
        if values:
            return min(values)
        return None

    def get_closest(self, timestamp, lookup_range=None):
        if not timestamp:
            return None
        before = None
        after = None
        data = self.get_timestamped_data(lookup_range, timestamp - 1)
        if data:
            before = data[-1]
        if before and timestamp == time_utils.unix_time(before[0]):
            return before
        data = self.get_timestamped_data(lookup_range,
            timestamp + lookup_range - 1)
        if data:
            after = data[0]
        if not before and not after:
            return None
        if before and not after:
            return before
        if after and not before:
            return after
        diff_before = timestamp - time_utils.unix_time(before[0])
        diff_after = time_utils.unix_time(after[0]) - timestamp
        if diff_before <= diff_after:
            return before
        else:
            return after
