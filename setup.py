#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
    biomon - Signs monitoring and patient management application

    Copyright (C) 2015 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import sys
import os
import subprocess

from setuptools import setup, find_packages
from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.cmd import Command


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management.commands.compilemessages import \
                    compile_messages
            for path, dirs, files in os.walk('src'):
                if 'locale' not in dirs:
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                compile_messages(sys.stderr)
                os.chdir(curdir)
        except ImportError:
            print
            sys.stderr.write('!!! Please install Django >= 1.8.2 to build translations')
            print
            print


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands

class eo_sdist(sdist):

    def run(self):
        print "creating VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print "removing VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')

class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)

def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--dirty','--match=v*'],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            return result.split()[0][1:].replace('-', '.')
        else:
            return '0.0.0-%s' % len(
                    subprocess.check_output(
                            ['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0.0'

README = file(os.path.join(
    os.path.dirname(__file__),
    'README')).read()

setup(name='biomon',
        version=get_version(),
        license='AGPLv3',
        description='VEADISTA Monitoring software',
        long_description=README,
        author="Entr'ouvert",
        url='https://repos.entrouvert.org/biomon.git',
        author_email="info@entrouvert.com",
        maintainer="Mikaël Ates",
        maintainer_email="mates@entrouvert.com",
        scripts=('biomon-ctl',),
        packages=find_packages('src'),
        package_dir={
            '': 'src',
        },
        package_data={
            'biomon': [
                  'templates/biomon/*.html',
                  'static/biomon/js/*.js',
                  'static/biomon/css/*.css',
                  'static/biomon/img/*.png',
                  'static/biomon/img/*.jpg',
                  'static/biomon/img/*.gif',
                  'static/biomon/img/bg/*.png',
                  'static/biomon/img/bg/*.jpg',
                  'static/biomon/img/bg/*.gif',
                  'locale/fr/LC_MESSAGES/django.po',
                  'locale/fr/LC_MESSAGES/django.mo',
            ],
        },
        install_requires=[
            'django=1.8.2',
            'matplotlib=1.4.3',
            'whisper=0.9.13',
            'simplejson=3.8.0',
            'requests=2.9.1',
        ],
        cmdclass={
            'build': build,
            'install_lib': install_lib,
            'compile_translations': compile_translations,
            'sdist': eo_sdist},

)
